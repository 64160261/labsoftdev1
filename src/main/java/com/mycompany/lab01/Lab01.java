/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab01;

/**
 *
 * @author ADMIN
 */

import java.util.Scanner;
public class Lab01 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char[][] board;
        char player;
        
        board = new char[3][3];
        player = 'X';
        
        // initial board empty
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
        // title board
        System.out.println("Welcome to XO Game");
        
        // loop play game
        while(true){
            
            System.out.println("Player " + player + " turn : ");
            int row = sc.nextInt();
            int col = sc.nextInt();
            
            // check box empty
            if(row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '-'){
                board[row][col] = player;
            
            // print board game
            for (int i = 0; i < 3; i++) {
                        for (int j = 0; j < 3; j++) {
                            System.out.print(board[i][j] + " ");
                        }
                        System.out.println();
            }   
            // loop check box empty
                boolean status = true;
                for(int i = 0;i < 3; i++){
                    for(int j = 0;j < 3; j++){
                        if(board[i][j] == '-'){
                            status = false;
                        }
                    }
                }
                // check player win
                if((board[row][0] == board[row][1] && board[row][1] == board[row][2]) || (board[0][col] == board[1][col] && board[1][col] == board[2][col]) || ( row == col && board[0][0] == board[1][1] && board[1][1] == board[2][2]) || ( row + col == 2 && board[2][0] == board[1][1] && board[1][1] == board[0][2])){
                    System.out.println("Player " + player + " WIN");
                    break;
                }
                
                // if board full => draw 
                else if(status){
                    System.out.println("It’s a draw!");
                    break;
                }
                
                // change player
                if(player == 'X'){
                    player = 'O';
                }else {
                    player = 'X';
                }
            
        }  
             
            // error input
            else{
                System.out.println("Invalid move. Try again.");
            }
        }
    }
        
         
}
